(ns clj-pbt.vec.math.my-math)

(defn arithmetic-mean
  "Calcola la media aritmetica dei numeri forniti"
  [& args]
  (/ (apply + args)
     (count args)))

(def ^:dynamic *delta* 1)

(defn- calculate-delta [n g] (->> (* g g) (- n) long Math/abs))

(defn sqrt
  "Calcola la radice quadrata di un dato intero"
  [n]
  (when (neg? n) (throw (ArithmeticException. "Input must be >= 0")))
  (loop [curr-guess 1
         prev-guess 0]
    (let [delta (calculate-delta n curr-guess)
          guess- (dec curr-guess)
          guess+ (inc curr-guess)]
      (if (or (<= delta *delta*)
              (= prev-guess curr-guess))
        (long (or (and (<= (calculate-delta n guess-) delta) guess-)
                  (and (<= (calculate-delta n guess+) delta) guess+)
                  curr-guess))
        (recur (long (arithmetic-mean curr-guess (/ n curr-guess)))
               curr-guess)))))
