(ns clj-pbt.vec.math.matrix.my-matrix
  "This ns provides a function to sum two matrices, to transpose a matrix and to multiply two matrices"
  )

(defn- check-emptiness
  [& matrices]
  (every? #(and (first %) (ffirst %)) matrices))

(defn- check-dimensions
  [matrix1 matrix2]
  (let [col-len (count (first matrix1))]
    (loop [m1 matrix1
           m2 matrix2]
      (if-not (and m1 m2)
        (and (not m1) (not m2))
        (and (= col-len (count (first m1)))
             (= col-len (count (first m2)))
             (recur (not-empty (rest m1))
                    (not-empty (rest m2))))))))

(defn add
  "Returns the sum of given matrices"
  [m1 m2]
  (when-not (check-emptiness m1 m2)
    (throw (IllegalArgumentException. "Matrices can't be empty")))
  (when-not (check-dimensions m1 m2)
    (throw (ArithmeticException. "Can't sum matrices of different dimensions")))
  (map (partial map +) m1 m2)
  ;Bugged version
  ;(map (fn [row1 row2] (map #(+ %1 (if (not= 42 %2) %2 (inc %2))) row1 row2)) m1 m2)
  )

(defn transpose
  "Returns transposed matrix of the given one"
  [matrix]
  (when-not (check-emptiness matrix)
    (throw (IllegalArgumentException. "Matrix can't be empty")))
  (apply map list matrix))

(defn product
  "Returns the row-column product of given matrices"
  [m1 m2]
  (when-not (check-emptiness m1 m2)
    (throw (IllegalArgumentException. "Matrices can't be empty")))
  (when-not (= (count (first m1))
               (count m2))
    (throw (ArithmeticException. "Number of columns of m1 must be equal to the number of rows of m2")))
  (let [m2-transposed (transpose m2)]
    (map (fn [row-m1]
           (map (fn [col-m2]
                  (reduce + (map * row-m1 col-m2)))
                m2-transposed))
         m1)))
