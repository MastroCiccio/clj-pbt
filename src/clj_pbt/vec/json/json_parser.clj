(ns clj-pbt.vec.json.json-parser
  "This ns publish just a function to transform Clojure structures to JSON strings.
  The result of the transformation can be verified using as model the function clojure.data.json/write-str"
  (:import (clojure.lang IPersistentCollection IPersistentMap)))

(declare clj->json clj->json-key clj->json-key')

(defn- clj-coll->json-key
  [coll]
  (->> (map clj->json-key' coll) (interpose " ") (apply str)))

(defn- map->json-key
  [^IPersistentMap coll]
  (str "{"
       (->> (map (fn [[k v]]  (str (clj->json-key' k)
                                   " "
                                   (clj->json-key' v)
                                   ;Bugged version
                                   ;(clj->json-key' v :parse-keyword? false)
                                   ))
                 coll)
            (interpose ", ")
            (apply str))
       "}"))

(defn- clj->json-key'
  [k & {parse-keyword? :parse-keyword? :or {parse-keyword? false}}]
  (cond (map? k) (map->json-key k)
        (vector? k) (str "[" (clj-coll->json-key k) "]")
        (list? k) (str "(" (clj-coll->json-key k) ")")
        (set? k) (str "#{" (clj-coll->json-key k) "}")
        (keyword? k) (or (and parse-keyword? (name k)) (pr-str k))
        :else (pr-str k)))

(defn- clj->json-key
  [k]
  (pr-str (or (and (string? k) k)
              (clj->json-key' k :parse-keyword? true))))

(defn- map->json
  [^IPersistentMap coll]
  (str "{"
       (->> (map (fn [[k v]]  (str (clj->json-key k) ":" (clj->json v)))
                 coll)
            (interpose ",")
            (apply str))
       "}"))

(defn- coll->json
  [^IPersistentCollection coll]
  (str "["
       (->> (map clj->json coll)
            (interpose ",")
            (apply str))
       "]"))

(defn clj->json
  "Transforms a clojure data type to a json string"
  [x]
  (cond ((some-fn boolean? number? string?) x) (pr-str x)
        (keyword? x) (pr-str (name x))
        (map? x) (map->json x)
        (coll? x) (coll->json x)
        :else (throw (IllegalArgumentException. "Can't translate input 'x' to json"))))
