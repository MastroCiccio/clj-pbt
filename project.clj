(defproject clj-pbt
  "0.4.0"
  :description "PBT with clojure and test.check"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]]
  :target-path "target/%s"
  :profiles {:dev {:dependencies [[org.clojure/data.json "1.0.0"]
                                  [org.clojure/test.check "1.0.0"]
                                  [com.gfredericks/test.chuck "0.2.10"]
                                  [com.taoensso/tufte "2.1.0"]]}
             :user {:plugins [[lein-nsorg "0.3.0"]
                              [lein-set-version "0.4.1"]]}}
  :repl-options {:port 4242
                 :timeout 10000})
