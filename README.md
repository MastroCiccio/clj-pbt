# Clojure - PBT & spec 

### Risorse

* Sito ufficiale Clojure: https://clojure.org/about/rationale 
* Clojure spec: https://clojure.org/about/spec
* WHAT CLOJURE SPEC IS AND WHAT YOU CAN DO WITH IT: https://www.pixelated-noise.com/blog/2020/09/10/what-spec-is/
* Repository test.check: https://github.com/clojure/test.check
* test.check issues: https://clojure.atlassian.net/projects/TCHECK/issues/?filter=allissues
* Leiningen: https://leiningen.org/

### Clojure in breve

* Linguaggio funzionale, tipizzato dinamicamente
* Dialetto LISP, è tutto rappresentato tramite liste di "oggetti" (tipi primitivi, symbols, liste innestate)
* Compila in bytecode per la JVM, interoperabilità con Java, gestione eccezioni
* REPL, anche per produzione, compilazione on the fly
* Il modo più semplice di usare Clojure è attraverso `Leiningen`: dipendenze, test, build, plugins, ...
* Notazione prefissa
* Poche strutture dati (immutabili, eterogenee), moltissime funzioni di manipolazione:
    * principali strutture dati: `LazySeq` e `PersistentArrayMap` (mappe)
    * principali pattern per l'elaborazione: `map` - `filter` - `reduce`, e simili
* Potente meccanismo di macro (vedi [AboutMacros](/AboutMacros.md))
* Codice organizzato in namespace
* No pattern matching, ma potente meccanismo di destrutturazione

#### Qualche esempio

```clojure
(println "print function")
print function
=> nil
```
___

```clojure
[0 1 2 3]    ;array
'(0 1 2 3)   ;list
{:k "value"} ;map
{:k "value" :xs [0 1 2] "ys" [0 "42" 1]}
```
___

```clojure
(get {:a 0} :a)
=> 0

(get-in {:a {:b {:c 0}}} [:a :b])
=> {:c 0}

({:b 1} :b)
=> 1
```
___

```clojure
; Interoperabilità con Java
(* (Integer/parseInt "42") 100)
=> 4200

; Attenzione alle eccezioni
(/ 42 0)
Execution error (ArithmeticException) at user/eval1521 (form-init15923127152044356411.clj:1).
Divide by zero

(try (/ 42 0) (catch ArithmeticException e (.printStackTrace e)))
java.lang.ArithmeticException: Divide by zero
	at clojure.lang.Numbers.divide(Numbers.java:188)
	at clojure.lang.Numbers.divide(Numbers.java:3901)
	at user$eval1513.invokeStatic(form-init1522690004125646450.clj:1)
	at user$eval1513.invoke(form-init1522690004125646450.clj:1)
	at clojure.lang.Compiler.eval(Compiler.java:7176)
	at clojure.lang.Compiler.eval(Compiler.java:7131)
	at clojure.core$eval.invokeStatic(core.clj:3214)
	at clojure.core$eval.invoke(core.clj:3210)
	at clojure.main$repl$read_eval_print__9068$fn__9071.invoke(main.clj:414)
	at clojure.main$repl$read_eval_print__9068.invoke(main.clj:414)
	at clojure.main$repl$fn__9077.invoke(main.clj:435)
	at clojure.main$repl.invokeStatic(main.clj:435)
	at clojure.main$repl.doInvoke(main.clj:345)
    ...
=> nil
```
___

```clojure
; funzioni anonime
(map inc (range 10))
=> (1 2 3 4 5 6 7 8 9 10)

(map #(inc %) (range 10))
=> (1 2 3 4 5 6 7 8 9 10)

(map (fn [n] (inc n)) (range 10))
=> (1 2 3 4 5 6 7 8 9 10)
```
___

```clojure
; composizione di funzioni
(map (comp str inc) (range 5))
=> ("1" "2" "3" "4" "5")

; funzioni parziali
(map (partial str "foo") (range 5))
=> ("foo0" "foo1" "foo2" "foo3" "foo4")

(map (comp (partial str "foo") inc) (range 5))
=> ("foo1" "foo2" "foo3" "foo4" "foo5")
```
___

La keyword `def` permette di aggiungere al namespace un nuovo simbolo e di legarlo (binding) a qualcosa,
i.e. un numero, una sequence, un altro simbolo, una funzione, etc.

```clojure
; Attenzione, non è un assegnamento, ma un binding della label `const` al numero 42
(def const 42)
=> #'user/const
```

La keyword `defn` ti permette di aggiungere al namespace una nuova funzione.
Zucchero sintattico per `(def func (fn [...] ...))`

```clojure
; Nota: nessuno ci garantisce che gli elementi di coll siano dei numeri o che coll stessa sia effettivamente un iterabile.
(defn mean
  "Calcola la media aritmetica degli elementi in coll"
  [coll]
  (/ (reduce + 0 coll) (count coll)))
=> #'user/mean

; In alternativa alla reduce
(defn mean
  "Calcola la media aritmetica degli elementi in coll"
  [coll]
  (/ (apply + 0 coll) (count coll)))
=> #'user/mean
```

La funzione `apply` prende come primo parametro una funzione e la applica alla lista di argomenti.
Molto utile nei casi in cui la funzione da applicare accetta un numero arbitrario di parametri.

```clojure
(apply + (range 11))
(apply + 0 (range 11))
(apply + 0 1 2 3 (range 4 11))
``` 

In clojure esistono molti modi di effettuare un binding, il più comune è usando la keyword `let`
```clojure
(defn split-even-and-odd
  [params]
  (let [evens (filter even? params)
        odds (filter odd? params)]
    (assoc {} :even evens :odd odds)))
=> #'user/split-even-and-odd

(split-even-and-odd (range 6))
=> {:even (0 2 4), :odd (1 3 5)}
```

Clojure non fornisce un costrutto per fare pattern matching, ma fornisce invece un meccanismo per destrutturare molto flessibile.

```clojure
; Destrutturazione di sequenze
(let [[x] (range 5)
      [x1 x2 & xs] (range 5)]
  (println x)
  (println x1 x2 xs))
0
0 1 (2 3 4)
=> nil

; Destrutturazione di mappe
(let [{x :x y :y} (assoc {} :x "x" :y "y")
      {label :x :as m} (assoc {} :x "foo" :y "bar") 
      ; :as permette di ottenere l'intera mappa a destra del binding
      {:keys [a b]} (assoc {} :a "alice" :b "bob")  
      ; :keys permette di listare le keyword di una mappa facendo un matching tra simbolo (dichiarato a sinistra del binding e keyword (contenuta nella mappa a destra del binding)
      {i :i j :j :or {j "default"}} (assoc {} :i "i")
      ; :or permette di elencare dei valori di default per quelle keyword non presenti nella mappa
      {{[a1 a2] :coll} :map} (assoc-in {} [:map :coll] [10 100])
      ; la destrutturazione può operare a qualsiasi livello di innestamento (il limite è solo la leggibilità)
      ]
  (println x y)
  (println label m)
  (println a b)
  (println i j)
  (println a1 a2))
x y
foo {:x foo, :y bar}
alice bob
i default
10 100
=> nil
```
___

Attenzione quando si usano le funzioni come `map` e `filter` in quanto ritornano in output una `LazySeq`.

```clojure
(time (map #(do (Thread/sleep 100) %) (range 10))) ;;la funzione map è lazy
"Elapsed time: 0.099913 msecs"
=> (0 1 2 3 4 5 6 7 8 9) ;questo viene stampato comunque dopo un secondo (ma solo perché siamo sul REPL)

(time (mapv #(do (Thread/sleep 100) %) (range 10)))
"Elapsed time: 1002.633355 msecs"
=> [0 1 2 3 4 5 6 7 8 9]
```

### Il testing in Clojure

Clojure fornisce nativamente la libreria per i test `clojure.test`.
Tale libreria espone la macro `is` che permette di fare asserzioni su espressioni booleane generiche.

```clojure
(is (= 5 (+ 2 2)))
```

Il fallimento, oltre a ritornare a sua volta il valore risultato dell'uguaglianza, stampa su console:
```
FAIL in ()
    expected: (= 5 (+ 2 2))
      actual: (not (= 5 4))
```

**NOTA**:
per eseguire asserzioni su codice di cui ci si aspetta il sollevamento di un'eccezione 
si possono utilizzare due forme speciali della macro `is`:
```clojure
(is (thrown? ArithmeticException (/ 1 0)))
(is (thrown-with-msg? ArithmeticException #"Divide by zero" (/ 1 0)))
```

#### Definizione del test

Uno dei modi con cui si può definire un test in clojure è usare la macro `deftest`:

```clojure
(deftest addition
  (is (= 4 (+ 2 2)))
  (is (= 7 (+ 3 4))))
``` 
Il codice soprastante definisce un test `addition` che può essere lanciato come una qualsiasi altra funzione.

La macro può anche essere usata per raggruppare test simili, come nel seguente esempio:

```clojure
(deftest arithmetic
  (addition)
  (subtraction))
```

### Il PBT con test.check

#### Definizione e verifica di una proprietà

```clojure
(require '[clojure.test.check.properties :as prop]
         '[clojure.test.check.generators :as gen])

(prop/for-all [v (gen/vector gen/nat)]
              (not-any? #{42} v))
```

La proprietà soprastante verifica che in un vettore `v` di naturali generato casualmente non sia presente il numero 42.

Questa banale proprietà può essere verificata passandola come parametro alla funzione `quick-check`:

```clojure
(require '[clojure.test.check.properties :as prop]
         '[clojure.test.check.generators :as gen]
         '[clojure.test.check :as tc])

(tc/quick-check 1000
                (prop/for-all [v (gen/vector gen/nat)]
                              (not-any? #{42} v)))
```

Questo codice esegue 1000 volte la proprietà passata come secondo argomento e, se non trova controesempi, ritorna una mappa come questa:

```clojure
{:result true, :pass? true, :num-tests 1000, :time-elapsed-ms 1, :seed 1592668297733}
```

Viceversa, in caso di fallimento, la mappa risultante ha la forma:

```clojure
{:shrunk {:total-nodes-visited 17,
          :depth 5,
          :pass? false,
          :result false,
          :result-data nil,
          :time-shrinking-ms 1,
          :smallest [[42]]},
 :failed-after-ms 2,
 :num-tests 43,
 :seed 1592670093346,
 :fail [[1 29 29 11 0 5 4 0 17 5 40 6 23 35 24 23 16 31 28 29 19 24 31 3 16 41 24 0 42 31 40 41 31]],
 :result false,
 :result-data nil,
 :failing-size 42,
 :pass? false}
```

La chiave `fail` mostra il primo controesempio trovato, mentre la chiave `shrunk` riporta i risultati dello shrinking.

#### Generatori

test.check offre una vasta scelta di 
[generatori standard](https://github.com/clojure/test.check/blob/master/doc/generator-examples.md): 
* per i numeri: `gen/nat`, `gen/large-integer`, `gen/double`, ...
* per le collection: `gen/vector`, `gen/tuple`, ...
* altri: `gen/map`, ``gen/one-of``, `gen/string-alfanumeric`, ...

Molti di questi generatori, come `gen/vector`, possono essere combinati tra di loro per costruire generatori più complessi:

```clojure
(gen/tuple (gen/vector gen/small-integer)
           (gen/list gen/string-alphanumeric))
```

##### `gen/fmap`

Per eseguire un mapping sui valori generati a partire da un altro generatore si può usare la funzione `fmap`:

```clojure
(gen/fmap (fn [n] (* n 2)) gen/nat)
```

##### `gen/such-that`

Similmente alle assunzioni di jqwick e quick-theories anche test.check tramite la funzione `such-that` 
permette di _filtrare_ i valori creati dai generatori:

```clojure
(gen/such-that (fn [n] (zero? (mod n 2))) gen/nat)
```

**NOTA**: un predicato troppo stringente potrebbe causare il fallimento del test per l'impossibilità di generare un numero sufficiente di valori.

##### `gen/bind`

La funzione `gen/bind` permette di costruire un generatore che dipende dai valori prodotti da un secondo generatore.
Ad esempio, se si vuole generare array di lunghezza compresa tra 3 e 5 si potrebbe innanzitutto far generare un intero
compreso in quell'intervallo e in seguito usarlo per costruire l'array:

```clojure
(gen/bind (gen/elements [3 4 5])
          (fn [n] (gen/vector gen/small-integer n)))
``` 

##### Generatori ricorsivi

Per la costruzione di generatori ricorsivi test.check offre la funzione `gen/recursive-gen` che richiede due parametri: 
1. un generatore "compound" (o "composite") dal quale ottenere strutture dati ricorsive,
2. un generatore di valori "scalari".

```clojure
(gen/sample (gen/recursive-gen gen/vector (gen/one-of [gen/nat gen/boolean])))

(gen/sample (gen/recursive-gen (fn [scalar-gen]
                                 (gen/one-of [(gen/vector scalar-gen) (gen/map scalar-gen scalar-gen)]))
                               (gen/one-of [gen/string-alphanumeric gen/double])))
```

Il primo esempio mostra un generatore le cui strutture dati prodotte avranno la forma di array di array di naturali o booleani.

Nel secondo esempio invece possono essere generati array di mappe (o viceversa) di stringhe o double.
La scelta di come combinare tra loro i generatori compound e quelli scalari è totalmente affidata alla libreria.

Nel namespace `clj-pbt.vec.json.json-parser-test` sono presenti un paio di esempi di uso dei generatori ricorsivi.
Interessante notare come funzioni bene lo shrinking (dopo aver causato opportunamente il fallimento del test) 
su una struttura ad albero complessa con vari livelli di innestamento.

### Il parametro `size`

[Growth and shrinking in test.check](https://github.com/clojure/test.check/blob/master/doc/growth-and-shrinking.md)

Il modo con cui test.check "controlla" internamente la generazione dei dati è quello di passare all'invocazione 
del generatore un paramentro `size` - oltre ad un secondo parametro per il random generator.
La size definisce una sorta di limite all'ampiezza del dominio di valori da cui il random generator scegli i dati.

Il valore del parametro size cresce linearmente - partendo da 0 - con il numero di invocazioni del generatore.
L'idea è quella di iniziare con la generazione di valori piccoli e via via aumentare la dimensione dell'input con il progredire dei casi di test.

```clojure
(gen/sample gen/nat 40)
;; => (0 1 2 2 1 4 6 4 8 0 8 5 9 9 14 3 9 3 10 17 3 14 10 11 8 4 10 17 16 7 5 27 2 25 22 12 13 22 24 22)

(gen/sample (gen/vector gen/nat) 20)
;; =>
;; ([]
;;  [1]
;;  []
;;  [2 3]
;;  []
;;  [0 3 3 5]
;;  [2]
;;  [4 3 2]
;;  [5 1 3 8 4]
;;  [9]
;;  [1 3 4 5 4 10 8]
;;  [8 11 7]
;;  [5 11 12 1]
;;  [9 0 12 4 4 8 10]
;;  [9 3 0 0 5 14 9 11 2 10]
;;  [12 13 1]
;;  [15 3 2 15]
;;  [9 14 12 15 14]
;;  [0 7 10 15 2 16 7 17 14 11 14 2 14]
;;  [10 6 7 16 19 15 3 19 14 0 17 14 3 14])
```

Nel primo esempio vengono eseguite 40 invocazioni del generatore per i numeri naturali. 
Si può facilmente notare come la seconda metà dei risultati (diciamo dal ventesimo in poi) i numeri prodotti siano
decisamente più grandi di quelli prodotti nella prima metà (con size da 0 a 20).

Nel secondo esempio invece vengono eseguite 20 invocazioni del generatore di array di numeri naturali.
In questo caso si osserva come la grandezza dei valori generati aumenti progressivamente 
sia rispetto alla dimensione degli interi, sia rispetto alla lunghezza degli array.

**ATTENZIONE**: di default la dimensione massima che viene passata all'invocazione di un generatore è pari a 200!

Ciò significa che anche su 100.000 iterazioni del generatore è altamente improbabile che i valori generati superino una certa soglia.
La soglia dipende dal generatore. Alcuni generatori ignorano del tutto la size.

```clojure
(apply max (gen/sample gen/nat 100000))
;; => 199
```

Esistono due modi per alterare il valore del parametro size: 
... vedi **esempi** in fondo al namespace `clj-pbt.vec.math.matrix.my-matrix-test` (Playing with "size scaling" and shrinking)

### Shrinking e `bind`

Come discusso sulla documentazione di test.check (vedi link paragrafo precedente), uno dei principali problemi
che la libreria affronta è dovuto allo shrinking dei generatori ottenuti con la funzione `bind`.

L'esempio del generatore di matrici evidenzia chiaramente le difficoltà dello shrinking in una situazione simile:
... vedi **esempi** in fondo al namespace `clj-pbt.vec.math.matrix.my-matrix-test` (Playing with "size scaling" and shrinking)

Il problema dell'esempio nasce dal fatto che il generatore degli array viene chiamato con una dimensione fissa
e pertanto non può essere ridotto dallo shrinking. L'unico modo per farlo è quello di ridurre invece l'intero che 
determina la dimensione dell'array generato, ma così facendo stiamo costruendo un nuovo generatore che
non è detto sia in grado di rilevare lo stesso problema!

### Integrazione tra `test.check` e `clojure.test`

Il modo con cui test.check si interfaccia con clojure.test si limita alla macro `defspec` che permette
di generare una funzione che il runner dei test riconosce come tale. 

```clojure
(require '[clojure.test.check.clojure-test :refer [defspec]]
         '[clojure.test.check.properties :as prop]
         '[clojure.test.check.generators :as gen])

(defspec sort-do-not-change-vector-dimension
         {:num-tests 1000}
         (prop/for-all [vect (gen/vector gen/large-integer)]
                       (= (count vect) (count (sort vect)))))
```

##### Integrazione con `(is (thrown? ...))`

Di fatto, test.check non è ben integrato con le asserzioni di clojure.test e ciò risulta evidente quando si tenta
di verificare che il SUT lanci una determinata eccezione per un certo insieme di input.

La forma speciale `(is (thrown? ...))` ritorna come risultato l'oggetto Throwable ottenuto stimolando il SUT.
La verifica della proprietà pertanto fallisce immediatamente pur se il risultato dell'asserzione ha dato esito positivo!
Per ovviare a questo problema bisogna di conseguenza wrappare la forma `(is (thrown? ...))` 
con un predicato che mappi il risultato della macro in un valore di verità che non faccia fallire la proprietà.

```clojure
(prop/for-all [n gen/nat] 
              (not (nil? (is (thrown? ArithmeticException (/ n 0))))))
```

Il predicato `nil?` rimappa la ArithmeticException sul valore `false` facendo sì che la proprietà non fallisca.

La situazione è ancora peggiore quando si vuole usare la forma speciale `(is (thrown-with-msg? ...))`.
Con questa forma i casi di fallimento possono riguardare anche il messaggio dell'eccezione.

```clojure
(prop/for-all [n gen/nat]
              (not (nil? (is (thrown-with-msg? ArithmeticException
                                               #"Divide by zerooo"
                                               (/ n 0))))))
```

La proprietà risulta verificata sebbene l'asserzione fallisca a causa del mismatch sul messaggio.
Questo avviene poiché il risultato della `is` è comunque una ArithmeticException, che non falsifica il predicato,
mentre il fallimento si evince dal messaggio di errore stampato come side effect.

##### Integrazione con `test.chuck`

La libreria [test.chuck](https://github.com/gfredericks/test.chuck) offre delle funzioni di helper che,
tra le altre cose, si prestano maggiormente ai casi in cui si vuole testare il sollevamento di eccezioni.

Ad esempio, la precedente proprietà si comporta molto meglio se riscritta usando la macro `for-all` di test.chuck:
```clojure
(require '[com.gfredericks.test.chuck.clojure-test :refer [for-all]])

(for-all [n gen/large-integer]
         (is (thrown? ArithmeticException (/ n 0)))
         (is (thrown-with-msg? ArithmeticException #"Divide by zerooo" (/ n 0))))
```

La prima asserzione dà sempre risultato positivo.
La seconda invece fa fallire la proprietà (questa volta ragionevolmente) e ritorna la classica mappa con il
controesempio e il risultato dello shrinking. 