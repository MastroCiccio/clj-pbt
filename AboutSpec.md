### Clojure Spec

Clojure non tipa staticamente i parametri o i valori di ritorno delle funzioni.
La libreria **Spec** nasce in risposta all'esigenza di strutturare in modo un po' "più" formale input e output delle varie funzioni.

Spec **non** aggiunge nessun controllo statico al programma (nessuna modifica al compilator),
rimanendo fedele alla filosofia del linguaggio basato sui tipi dinamici.

Spec fornisce degli strumenti dichiarativi per descrivere i dati, i loro tipi e la loro forma, 
il tutto sempre disponibile a run-time.

Spec è ancora in alpha (si attende spec 2) anche se già integrata in Clojure. 

#### Predicati

Nel contesto della libreria, una `spec` non è altro che un predicato che definisce una serie di valori permessi.

```clojure
(s/valid? even? 10)
(s/valid? nil? nil) 
(s/valid? string? "abc")
(s/valid? #(> % 5) 10)
(s/valid? #(> % 5) 0)

(s/conform #(> % 5) 10)
(s/conform #(> % 5) 0)
```

Negli esempi precedenti, il primo parametro è sempre un predicato che viene trasformato dalla libreria in una spec.
Il secondo parametro, invece, è l'input da dare alla spec al fine di ottenere il risultato della validazione.

Sia `valid?` che `conform` sono entrambi modi per validare un certo input.
`valid?` si limita a ritornare un valore di verità sulla base del predicato.
`conform`, invece, ritorna:
* l'input "conformato", se il predicato ritorna una valore truthy (sul significato di "conformed" ci torniamo fra un attimo)
* la keyword `:clojure.spec.alpha/invalid`, altrimenti

Per definire una spec:
```clojure
(s/def ::username string?)
```

L'espressione `::username` è una fully qualified keyword, ovvero comprende, come prefisso, il namespace in cui è definita.
Nel caso delle spec, è consuetudine usare le fully qualified keyword per evitare possibili collisioni tra diversi namespace.

Naturalmente, si può definire una spec a partire dalla composizione di più predicati:

```clojure
(s/def ::not-empty-username (s/and ::username #(> (count %) 0)))
(s/conform ::not-empty-username "Metodi formali")
=> "Metodi formali"
(s/conform ::not-empty-username "")
=> :clojure.spec.alpha/invalid

(s/def ::int-or-string (s/or :string string?
                             :int    int?))
(s/conform ::int-or-string "42")
=> [:string "42"]
(s/conform ::int-or-string 42)
=> [:int 42]
``` 

Come si nota dagli ultimi due esempi, il risultato della funzione `conform` è una struttura dati che non ci aspettavamo.
Una spec non si limita soltanto a validare, ma anche a fare il parsing (conform) dell'input, allo scopo di produrre
in output una struttura dati ben definita.

Nell'esempio, l'output `[:int 42]` ci sta comunicando che l'input `42` è stato sì validato, 
ma soprattuto che è stato validato dal predicato associato alla keyword `:int` nella definizione della spec.

Non tutte le spec effettuano questa operazione di parsing, basti pensare alla spec `::username`, non c'è molto di cui fare il parsing... 

Ulteriori esempi di spec nel namespace `clj-pbt.vec.spec.presentation`.

#### Un caso d'uso - Property Based Testing

Un possibile uso delle spec è quello di usarle per fare un po' di PBT.
Per farlo basta ricorrere alla funzione `s/fdef`, la quale permette di definire, per una certa funzione, le seguenti spec:
* `:args`: permette di definire che tipo di dati si aspetta in input la nostra funzione
* `:ret`: permette di definire il tipo di dati atteso in output
* `:fn`: permette di definire eventuali relazioni fra input e output della funzione

```clojure
(defn ranged-rand
  "Returns random int in range start <= rand < end"
  [start end]
  (+ start (long (rand (- end start)))))

(s/fdef ranged-rand
        :args (s/and (s/cat :start int? :end int?)
                     #(< (:start %) (:end %)))
        :ret int?
        :fn (s/and #(>= (:ret %) (-> % :args :start))
                   #(< (:ret %) (-> % :args :end))))
```

Una volta definito la nostra spec, è sufficiente invocare la funzione `check` del namespace `clojure.spec.test.alpha`
che si occuperà di fare tutto il lavoro.
Infatti, gli input alla funzione verranno automaticamente generati sfruttando la spec definita per `:args`.
Per ogni input generato, la funzione sotto test verrà invocata e così le spec `:ret` e `:fn` che valideranno l'output.

#### Altri casi d'uso

* Validation
* Error reporting
* Destructuring/parsing
* Instrumentation
* Test-data generation
* Infer specs from data -> https://github.com/stathissideris/spec-provider
* Gain confidence that different implementations are equivalent
* Validate an external system
* Documentation/communication

#### Ancora qualche problema

* spec è ancora in alpha, spec2 dovrebbe completamente rimpiazzarla in futuro
* errori particolarmente prolissi e difficili da interpretare
* openness of maps: non c'è un modo per validare una mappa in modo stringente (spec2 dovrebbe risolvere il problema)
* slow, non sembra adatto per l'ambiente di produzione
* non sembra esserci un modo di usare un generatore custom per il testing (a meno di non usare test.check)
* No metadata