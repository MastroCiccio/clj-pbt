(ns clj-pbt.vec.math.my-math-test
  (:require [clj-pbt.vec.math.my-math :as math]
            [clojure.test :refer [is]]
            [clojure.test.check :as tc]
            [clojure.test.check.clojure-test :refer [defspec]]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]))

(def default-seed 4242)
(def default-num-tests 10000)

(defn- calculate-delta [n g] (->> (* g g) (- n) long Math/abs))

(def perfect-squares-property
  (prop/for-all [n (gen/fmap #(* % %) gen/nat)]
                (->> (math/sqrt n) (calculate-delta n) zero?)
                #_(is (->> (math/sqrt n) (calculate-delta n) zero?))
                ;uses the is assertion if you want a slightly better failure report
                ;DRAWBACK: performance goes down a little bit
                ))

(defspec test-sqrt-with-perfect-squares
         {:num-tests default-num-tests :seed default-seed}
         perfect-squares-property)

;(tc/quick-check 10000 perfect-squares-property :seed default-seed)
;(test-sqrt-with-perfect-squares 10000 :seed default-seed)

(def negative-integers-property
  (prop/for-all [n (gen/fmap - (gen/such-that (comp not zero?) gen/nat))]
                (try (math/sqrt n)
                     (catch Exception e
                       (and (instance? ArithmeticException e)
                            (= "Input must be >= 0" (.getMessage e)))
                       #_(do (is (instance? ArithmeticException e))
                           (is (= "Input must be >= 0" (.getMessage e))))))))

(defspec test-sqrt-with-negatives
         {:num-tests default-num-tests :seed default-seed}
         negative-integers-property)

;(tc/quick-check 10000 negative-integers-property :seed default-seed)

(def positive-integers-property
  (prop/for-all [n (gen/large-integer* {:min 0 :max Integer/MAX_VALUE})]
                (let [g (math/sqrt n)
                      delta (calculate-delta n g)]
                  (or (<= delta math/*delta*)
                      (and (<= delta (calculate-delta n (dec g)))
                           (<= delta (calculate-delta n (inc g))))))))

(defspec test-sqrt-with-positive-integers
         {:num-tests default-num-tests :seed default-seed}
         positive-integers-property)

;(tc/quick-check 10000 positive-integers-property :seed default-seed)

(def sqrt-with-model-property
  (prop/for-all [n (gen/large-integer* {:min 0 :max Integer/MAX_VALUE})]
                (= (math/sqrt n)
                   (Math/round (Math/sqrt n)))))

(defspec test-sqrt-with-model
         {:num-tests default-num-tests :seed default-seed}
         sqrt-with-model-property)

;(tc/quick-check 10000 same-as-math-sqrt-property :seed default-seed)
