(ns clj-pbt.vec.math.matrix.my-matrix-test
  (:require [clj-pbt.vec.math.matrix.my-matrix :as matrix]
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.pprint :as pp]
            [clojure.test :refer [deftest is testing]]
            [clojure.test.check :as tc]
            [clojure.test.check.clojure-test :refer [defspec]]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]
            [com.gfredericks.test.chuck.clojure-test :refer [checking for-all]]
            [taoensso.tufte :as tufte :refer [p profile]]))

(tufte/add-basic-println-handler! {})

(def default-seed 4242)
(def default-num-tests 2000)
(def default-max-size 500)

(defn- forall?
  "Like every? but accepts several collections in input.
  The arity of pred must be equal to the number of the input collections."
  [pred & colls]
  (not (some false? (apply map pred colls))))

(defn- gen-pos-nat [] (gen/such-that pos? gen/nat))
(defn- bind-to-nat [f] (gen/bind (gen-pos-nat) f))
(defn- gen-row [m] (gen/vector gen/small-integer m))
(defn- gen-matrix [n m] (gen/vector (gen-row m) n))

(defn- matrix-generator
  []
  (gen/not-empty (bind-to-nat (fn [n] (gen/vector (gen-row n))))))

(defn- two-matrices-generator
  "Generate two matrices of the same size"
  []
  (bind-to-nat (fn [n]
                 (bind-to-nat (fn [m]
                                (gen/tuple (gen-matrix n m) (gen-matrix n m)))))))

(declare m m1 m2)

(def add-does-not-alter-dimensions-property
  (prop/for-all [[m1 m2] (two-matrices-generator)]
                ;;NOTA: the assertion reports either the first failure and the shrunk ones
                (let [res (matrix/add m1 m2)]
                  (and (is (= (count res)
                              (count m1)
                              (count m2))
                           "Summation must preserve rows number")
                       (is (= (count (first res))
                              (count (first m1))
                              (count (first m2)))
                           "Summation must preserve columns number")))))

;(tc/quick-check 1000 add-does-not-alter-dimensions-property :seed default-seed)

(defspec test-add-does-not-alter-dimensions
         {:num-tests default-num-tests :seed default-seed}
         add-does-not-alter-dimensions-property)

(def add-does-sum-position-per-position-property
  (prop/for-all [[m1 m2] (two-matrices-generator)]
                (let [res (matrix/add m1 m2)]
                  (is (forall? (fn [res-row m1-row m2-row]
                                 (forall? (fn [n i j]
                                            (= n (+ i j))
                                            ;(is (= n (+ i j)))
                                            )
                                          res-row m1-row m2-row))
                               res m1 m2)))))

;(tc/quick-check 1000 add-does-sum-position-per-position-property :seed default-seed)

(defspec test-add-does-sum-position-per-position
         {:num-tests default-num-tests :seed default-seed}
         add-does-sum-position-per-position-property)

(def transpose-flip-matrix-property
  (prop/for-all [m (matrix-generator)]
                (let [res (matrix/transpose m)
                      m-rows (range (count m))
                      m-cols (range (count (first m)))]
                  (is (forall? (fn [i m-row-i]
                                 (forall? (fn [j res-col-j]
                                            (= (nth m-row-i j)
                                               (nth res-col-j i))
                                            #_(is (= (nth m-row-i j)
                                                   (nth res-col-j i))))
                                          m-cols res))
                               m-rows m)))))

;(tc/quick-check 1000 transpose-flip-matrix-property :seed default-seed)

(defspec test-transpose-flip-matrix
         {:num-tests default-num-tests :seed default-seed}
         transpose-flip-matrix-property)

(def transpose-matrix-twice-result-to-matrix-property
  (prop/for-all [m (matrix-generator)]
                (is (= m (matrix/transpose (matrix/transpose m))))))

;(tc/quick-check 1000 transpose-matrix-twice-result-to-matrix-property :seed default-seed)

(defspec test-transpose-matrix-twice-result-to-matrix
         {:num-tests default-num-tests :seed default-seed :max-size default-max-size}
         transpose-matrix-twice-result-to-matrix-property)

(def matrices-product-dimension-property
  (prop/for-all [[m1 m2] (bind-to-nat (fn [n]
                                        (gen/tuple (gen/not-empty (gen/vector (gen-row n)))
                                                   (bind-to-nat (fn [m] (gen-matrix n m))))))]
                (let [res (matrix/product m1 m2)]
                  (and (is (= (count res) (count m1)) "Result rows number must be equal to m1's rows number")
                       (is (= (count (first res)) (count (first m2)))
                           "Result cols number must be equal to m2's cols number")))))

;(tc/quick-check 1000 matrices-product-dimension-property :seed default-seed)

(defspec test-matrices-product-dimension
         {:num-tests default-num-tests :seed default-seed}
         matrices-product-dimension-property)

;;Proprietà metamorfica: la matrice prodotto trasposta è uguale al prodotto delle matrici trasposte
(def transpose-product-property
  (prop/for-all [[m1 m2] (bind-to-nat (fn [n]
                                        (gen/tuple (gen/not-empty (gen/vector (gen-row n)))
                                                   (bind-to-nat (fn [m] (gen-matrix n m))))))]
                (is (= (p :trans-prod (matrix/transpose (matrix/product m1 m2)))
                       (p :prod-trans (matrix/product (matrix/transpose m2) (matrix/transpose m1)))))))

;;NOTE: to let profiling to work properly you must realize the lazy sequences
;(profile {} (p :prop (tc/quick-check 200 transpose-product-property :seed default-seed)))

(defspec test-transpose-product
         {:num-tests 1000 :seed default-seed}
         transpose-product-property)

;;;;;;;;;;;;;;;;; What about exceptions?

(declare thrown? thrown-with-msg?)

#_(tc/quick-check 100
                (prop/for-all [n gen/large-integer]
                              ;(is (thrown? ArithmeticException (seq n)))
                              ;(is (thrown? IllegalArgumentException (seq n)))
                              ;(not (nil? (is (thrown? IllegalArgumentException (seq n)))))
                              (not (nil? (is (thrown-with-msg? IllegalArgumentException
                                                               #"Don't know how to create ISeq from: java.lang.Long"
                                                               (seq n)))))
                              ))

;;Questo test "passa", ma vengono comunque riportati 100 casi di fallimento...
#_(defspec test-transpose-with-empty-matrix
         {:num-tests 100 :seed default-seed}
         (prop/for-all [m (gen/one-of [(gen/return [])
                                       (gen/vector (gen/return []))])]
                       (is (Throwable->map (is (thrown-with-msg? IllegalArgumentException
                                                                 #"Matrix can't be empty"
                                                                 (matrix/transpose m)))))))

;(test-transpose-with-empty-matrix)

(deftest test-catch-exceptions
   (checking "That this works well with catching exceptions"
             {:num-tests 100 :seed default-seed}
             [n gen/large-integer]
             (is (thrown? ArithmeticException (/ n 0)))
             (is (thrown-with-msg? ArithmeticException #"Divide by zero" (/ n 0)))
             (is (thrown? IllegalArgumentException (seq n)))
             ))

;(test-catch-exceptions)

(defspec test-catch-exceptions-with-defspec
         {:num-tests 100 :seed default-seed}
         (for-all [n gen/large-integer]
                  (is (thrown? ArithmeticException (/ n 0)))
                  (is (thrown-with-msg? ArithmeticException #"Divide by zero" (/ n 0)))
                  (is (thrown? IllegalArgumentException (seq n)))))

;(test-catch-exceptions-with-defspec)

#_(tc/quick-check 100
                (for-all [n gen/large-integer]
                         (is (thrown? ArithmeticException (/ n 0)))
                         (is (thrown-with-msg? ArithmeticException #"aDivide by zero" (/ n 0)))
                         (is (thrown? IllegalArgumentException (seq n))))
                :seed default-seed)

;;;;;;;;;;;;;;;;; Playing with max-size

#_(let [trials 2000
      size 200
      group-size 50
      report-name (str "reports/report-" trials "-" size "-" group-size ".edn")]
  (spit report-name "")
  (tc/quick-check trials
                  (prop/for-all [m (matrix-generator)] true)
                  :seed default-seed
                  :max-size size
                  :reporter-fn (fn [{type :type [m] :args}]
                                 (when (= :trial type)
                                   (pp/pprint {:rows (count m)
                                               :cols (count (first m))}
                                              (io/writer report-name :append true)))))
  (let [report (edn/read-string (str "[" (slurp report-name) "]"))
        group-report {:grouped-by-rows (reduce-kv (fn [init k v]
                                                    (let [group-lower-bound (* group-size (int (/ k group-size)))
                                                          group-upper-bound (+ group-size group-lower-bound)]
                                                      (merge-with concat
                                                                init
                                                                {[group-lower-bound group-upper-bound]
                                                                 (map :cols v)})))
                                                  {} (group-by :rows report))
                      :grouped-by-cols (reduce-kv (fn [init k v]
                                                    (let [group-lower-bound (* group-size (int (/ k group-size)))
                                                          group-upper-bound (+ group-size group-lower-bound)]
                                                      (merge-with concat
                                                                  init
                                                                  {[group-lower-bound group-upper-bound]
                                                                   (map :rows v)})))
                                                  {} (group-by :cols report))}]
    (pp/pprint {:trials trials
                :size size
                :group-size group-size
                :seed default-seed
                :rows-distribution (reduce-kv (fn [init k v]
                                                (conj init {:num-rows  k
                                                            :count     (count v)
                                                            :frequency (-> (count v) (/ trials) (* 100.))}))
                                              [] (:grouped-by-rows group-report))
                :cols-distribution (reduce-kv (fn [init k v]
                                                (conj init {:num-cols  k
                                                            :count     (count v)
                                                            :frequency (-> (count v) (/ trials) (* 100.))}))
                                              [] (:grouped-by-cols group-report))
                }
               (io/writer report-name))))

;;;;;;;;;;;;;;;;; Playing with "size scaling" and shrinking

#_(let [result (tc/quick-check 1000
                             #_(prop/for-all [ll1 (gen/vector gen/small-integer)
                                            ll2 (gen/vector gen/small-integer)]
                                           (or (not-any? #{42} ll1)
                                               (not-any? #{42} ll2)))
                             (prop/for-all [ll (gen/bind (gen/fmap #(+ % 5) gen/nat) ;smallest [0 0 0 0 0 0 0 0 0 0 0 42 0]
                                                         (fn [n] (gen/vector gen/small-integer n)))]
                                           (not-any? #{42} ll))
                             :seed default-seed)]
  (-> (select-keys result [:pass? :num-tests :shrunk :failing-size :fail])
      (update-in [:shrunk :smallest] first)))

#_(let [result (tc/quick-check 1000
                             (prop/for-all [;m (matrix-generator)                        ;smallest [[42]]
                                            ;m (gen/scale #(* 2 %) (matrix-generator))   ;smallest [[42 0]]
                                            ;m (gen/scale #(+ 100 %) (matrix-generator)) ;smallest [[0 0 0 0 0 0 0 0 42]]
                                            ;m (gen/scale #(* 3 %) (matrix-generator))   ;smallest [[0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 42]]
                                            m (gen/scale #(* 5 %) (matrix-generator))   ;smallest [[0 0 42]]
                                            ]
                                           (every? (fn [row] (not-any? #{42} row)) m))
                             :seed default-seed
                             )]
  (-> (select-keys result [:pass? :num-tests :shrunk :failing-size :fail])
      (update-in [:shrunk :smallest] first)
      (update-in [:shrunk] #(assoc % :smallest-dim {:rows (count (get-in % [:smallest]))
                                                    :cols (count (get-in % [:smallest 0]))}))
      (update-in [:fail] (comp #(assoc {} :rows (count %)
                                          :cols (count (first %)))
                               first))))

#_(map count (gen/sample (gen/scale #(* 2 (+ % 200)) (matrix-generator))))
