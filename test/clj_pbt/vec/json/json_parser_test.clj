(ns clj-pbt.vec.json.json-parser-test
  (:require [clj-pbt.vec.json.json-parser :as jp]
            [clojure.data.json :as json]
            [clojure.test :refer [is]]
            [clojure.test.check :as tc]
            [clojure.test.check.clojure-test :refer [defspec]]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]))

(def default-seed 4242)
(def default-num-tests 2000)
(def default-max-size 500)

(defn- string-generator [max-len] (gen/fmap (fn [s] (subs s 0 (mod (count s) max-len))) gen/string-alphanumeric))

(def ^:private scalar-gens (gen/one-of [(string-generator 40)
                                        gen/small-integer
                                        gen/boolean
                                        (gen/fmap keyword (gen/not-empty (string-generator 20)))]))

(def ^:private coll-gens (fn [scalar-gen]
                           (gen/one-of [(gen/vector scalar-gen)
                                        (gen/list scalar-gen)
                                        (gen/set scalar-gen)])))

(def ^:private compound-gens (fn [scalar-gen]
                               (gen/one-of [(gen/vector scalar-gen)
                                            (gen/list scalar-gen)
                                            (gen/set scalar-gen)
                                            (gen/map scalar-gen scalar-gen)])))

(def string-array-to-json-property
  (prop/for-all [v (gen/recursive-gen gen/vector scalar-gens)]
                (is (= (jp/clj->json v)
                       (json/write-str v)))))

(defspec test-string-array-to-json-property
         {:num-tests default-num-tests :seed default-seed :max-size default-max-size}
         string-array-to-json-property)

(def colls-to-json-property
  (prop/for-all [v (gen/recursive-gen coll-gens scalar-gens)]
                (is (= (jp/clj->json v)
                       (json/write-str v)))))

(defspec test-colls-to-json-property
         {:num-tests default-num-tests :seed default-seed :max-size default-max-size}
         colls-to-json-property)

(def data-to-json-property
  (prop/for-all [v (gen/recursive-gen compound-gens scalar-gens)]
                (is (= (jp/clj->json v)
                       (json/write-str v)))))

(defspec test-data-to-json-property
         {:num-tests default-num-tests :seed default-seed :max-size default-max-size}
         data-to-json-property)
