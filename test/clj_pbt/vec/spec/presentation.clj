(ns clj-pbt.vec.spec.presentation
  (:require [clojure.test :refer :all]
            [clojure.spec.alpha :as s]
            [clojure.spec.gen.alpha :as gen]
            [clojure.spec.test.alpha :as stest]))

(s/valid? number? 42) ; => true
(s/valid? string? 42) ; => false
(s/conform number? 42) ; => 42
(s/conform string? 42) ; => :clojure.spec.alpha/invalid

#_(s/valid? pos? nil)
; => Execution error (java.lang.NullPointerException)
;	at clojure.lang.Numbers.ops (Numbers.java:1068)
;	at clojure.lang.Numbers.isPos (Numbers.java:121)
;	at clojure.core$pos? (static) (core.clj:1252)
;	at clojure.core$pos? (core.clj:1247)
(s/valid? (s/nilable pos?) nil)

(s/def ::int-or-string (s/or :string string?
                             :int    int?))

(s/conform ::int-or-string "42")
(s/conform ::int-or-string 42)

(s/explain ::int-or-string 42.42)
; 42.42 - failed: string? at: [:string] spec: :clj-pbt.vec.spec.presentation/int-or-string
; 42.42 - failed: int? at: [:int] spec: :clj-pbt.vec.spec.presentation/int-or-string
(s/explain-data ::int-or-string 42.42)
; {:problems ({:path [:string],
;              :pred clojure.core/string?,
;              :val 42.42,
;              :via [:clj-pbt.vec.spec.presentation/int-or-string],
;              :in []}
;             {:path [:int],
;              :pred clojure.core/int?,
;              :val 42.42,
;              :via [:clj-pbt.vec.spec.presentation/int-or-string],
;              :in []}),
;  :spec :clj-pbt.vec.spec.presentation/int-or-string,
;  :value 42.42}

;; `s/coll-of` permette di validare liste omogenee di lunghezza variabile
(s/conform (s/coll-of int?) '(0 1 2 3 4 5))
(s/explain (s/coll-of number? :kind vector? :count 3 :distinct true :into #{}) [1 2 3 4])
; [1 2 3 4 4] - failed: (= 3 (count %))
(s/explain (s/coll-of number? :kind vector? :count 3 :distinct true :into #{}) [1 2 2])
; [1 2 2] - failed: distinct?
(s/explain (s/coll-of number? :kind vector? :count 3 :distinct true :into #{}) '(1 2 3))
; (1 2 3) - failed: vector?

;; `s/tuple` permetti di validare liste eterogenee di lunghezza fissata
(s/conform (s/tuple double? double? double?) [1.5 2.5 -0.5])  ; => [1.5 2.5 -0.5]
(s/conform (s/tuple double? double? double?) '(1.5 2.5 -0.5)) ; => :clojure.spec.alpha/invalid
(s/conform (s/tuple even? double? string?) [3 2.5 "string"])

;; le liste possono essere viste attraverso espressioni regolari
;; `s/cat`, `s/*`, `s/+`, `s/?` sono tutte spec che, combinate assieme, permettono di definire spec atte a validare liste di qualsiasi forma
(s/conform (s/* (s/cat :first int? :second double? :third (s/alt :string string? :nil nil?)))
           [0 1.5 "42"])
; => [{:first 0, :second 1.5, :third [:string "42"]}]
(s/conform (s/* (s/cat :first int? :second double? :third (s/alt :string string? :nil nil?)))
           [0 1.5 "42" 1 2.5 nil])
; => [{:first 0, :second 1.5, :third [:string "42"]} {:first 1, :second 2.5, :third [:nil nil]}]
(s/conform (s/* (s/cat :first int? :second double? :third (s/alt :string string? :nil nil?)))
           [0 1.5 nil])
; => [{:first 0, :second 1.5, :third [:nil nil]}]

(s/def ::odds-then-maybe-even (s/cat :odds (s/+ odd?) :even (s/? even?)))
(s/conform ::odds-then-maybe-even [1 3 5 100])
; => {:odds [1 3 5], :even 100}
(s/conform ::odds-then-maybe-even [1])
; => {:odds [1]}
(s/explain ::odds-then-maybe-even [100])
; 100 - failed: odd? in: [0] at: [:odds] spec: :user/odds-then-maybe-even

(def email-regex #"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,63}$")
(def phone-regex #"^(\+\d{2}\d{9,10})|(\d{9,10})$")
(s/def ::first-name string?)
(s/def ::last-name string?)
(s/def ::email (s/and string? #(re-matches email-regex %)))
(s/def ::phone (s/and string? #(re-matches phone-regex %)))

;; La funzione `s/keys` permette di creare una spec, con il seguente design:
;; - la spec non definisce il tipo dei valori della mappa, definiamo soltanto quali sono le entità che la compongono
;; - le keyword (fully qualified) all'interno della mappa dovranno essere le stesse con le quali abbiamo definito delle spec in precedenza
(s/def ::person (s/keys :req [::first-name ::last-name ::email]
                        :opt [::phone]))

(s/conform ::person {::first-name "Francesco" ::last-name "Arena" ::email "a@a.it"})
; => {:first-name "Francesco", :last-name "Arena", :email "a@a.it"}
(s/conform ::person {::first-name "Francesco" ::last-name "Arena" ::email "a@a.it" ::phone "024492228"})
; => {:first-name "Francesco", :last-name "Arena", :email "a@a.it", :phone "024492228"}

;; Attenzione, le spec per le mappe validano solo le keywords dichiarate! Tutto il resto viene ignorato
(s/conform ::person {::first-name "Francesco" ::last-name "Arena" ::email "a@a.it" ::height 177})
; => {:first-name "Francesco", :last-name "Arena", :email "a@a.it", :height 177}


;; Un caso d'uso interessante: un mini parser per le funzioni stesse di clojure
(s/def ::function (s/cat :defn #{'defn}
                         :name symbol?
                         :doc (s/? string?)
                         :args vector?
                         :body (s/+ list?)))

(def function-code1
  '(defn my-function
     "this is a test function"
     [x y]
     (+ x y)))

(s/conform ::function function-code1)
; => {:defn defn, :name my-function, :doc "this is a test function", :args [x y], :body [(+ x y)]}

;; Le spec possono essere usate all'interno come precondizioni e postcondizioni delle funzioni, ma occhio alle eccezioni
(defn person-name
  [person]
  {:pre [(s/valid? ::person person)]
   :post [(s/valid? string? %)]}
  (str (::first-name person) " " (::last-name person)))

#_(person-name {::first-name "Francesco" ::last-name "Arena"})
; java.lang.AssertionError: Assert failed: (s/valid? :clj-pbt.vec.spec.dummy/person person)

;;;;;;;;;;;; PBT con spec

(defn ranged-rand
  "Returns random int in range start <= rand < end"
  [start end]
  (+ start (long (rand (- end start)))))

(s/fdef ranged-rand
        :args (s/and (s/cat :start int? :end int?)
                     #(< (:start %) (:end %)))
        :ret int?
        :fn (s/and #(>= (:ret %) (-> % :args :start))
                   #(< (:ret %) (-> % :args :end))))

;(stest/check `ranged-rand)
;(stest/check `ranged-rand {:clojure.spec.test.check/opts {:num-tests 2000}})

;;;;;;;;;;;;

;; Testing a parte, è possibile instrumentare le nostre funzioni allo scopo di accertarsi che tutte le chiamate siano effettuate con input "corretti".
;; Per farlo, usiamo la funzione `instrument` che comunica a Clojure di invocare la spec `:args` (precedentemente definita con `fdef`) ad ogni chiamata della funzione/i che vogliamo controllare.
;; Attenzione, questo metodo non è consigliato per gli ambienti di produzione.
(stest/instrument `ranged-rand)
;(stest/unstrument `ranged-rand)

;; A questo punto, a tutte le successive chiamate di `ranged-rand` Clojure si assicurerà che gli input non siano scorretti.
#_(ranged-rand 1 10)
#_(ranged-rand 8 0)
; Execution error - invalid arguments to clj-pbt.vec.spec.dummy/ranged-rand at (dummy.clj:109).
; {:start 8, :end 0} - failed: (< (:start %) (:end %))

; clojure.lang.ExceptionInfo: Call to #'clj-pbt.vec.spec.dummy/ranged-rand did not conform to spec.
; {:clojure.spec.alpha/problems [{:path [],
;                                 :pred (clojure.core/fn [%] (clojure.core/< (:start %) (:end %))),
;                                 :val {:start 8, :end 0},
;                                 :via [],
;                                 :in []}],
;  :clojure.spec.alpha/spec #object[clojure.spec.alpha$and_spec_impl$reify__2183 0x5f885906 "clojure.spec.alpha$and_spec_impl$reify__2183@5f885906"],
;  :clojure.spec.alpha/value (8 0),
;  :clojure.spec.alpha/fn clj-pbt.vec.spec.dummy/ranged-rand,
;  :clojure.spec.alpha/args (8 0),
;  :clojure.spec.alpha/failure :instrument,
;  :clojure.spec.test.alpha/caller {:file "dummy.clj", :line 109, :var-scope clj-pbt.vec.spec.dummy/eval3082}}

(defn ranged-rand-broken
  "Returns random int in range start <= rand < end"
  [start end]
  (+ start (long (rand (- start end)))))

(s/fdef ranged-rand-broken
        :args (s/and (s/cat :start int? :end int?)
                     #(< (:start %) (:end %)))
        :ret int?
        :fn (s/and #(>= (:ret %) (-> % :args :start))
                   #(< (:ret %) (-> % :args :end))))

(stest/instrument `ranged-rand-broken)
(stest/abbrev-result (first (stest/check `ranged-rand-broken)))
; => {:spec (fspec
;             :args (and (cat :start int? :end int?) (fn* [p1__3468#] (< (:start p1__3468#) (:end p1__3468#))))
;             :ret int?
;             :fn (and
;                   (fn* [p1__3469#] (>= (:ret p1__3469#) (-> p1__3469# :args :start)))
;                   (fn* [p1__3470#] (< (:ret p1__3470#) (-> p1__3470# :args :end))))),
;      :sym spec.examples.guide/ranged-rand,
;      :result {:clojure.spec.alpha/problems [{:path [:fn],
;                                              :pred (>= (:ret %) (-> % :args :start)),
;                                              :val {:args {:start -3, :end 0}, :ret -5},
;                                              :via [],
;                                              :in []}],
;               :clojure.spec.test.alpha/args (-3 0),
;              :clojure.spec.test.alpha/val {:args {:start -3, :end 0}, :ret -5},
;              :clojure.spec.alpha/failure :test-failed}}

;;;;;;;;;;;; Generatori, wrapper dei generatori di test.check (nulla di nuovo!)

(gen/generate (s/gen int?))
; => -959
(gen/generate (s/gen nil?))
; => nil
(gen/sample (s/gen string?))
; => ("" "" "" "" "8" "W" "" "G74SmCm" "K9sL9" "82vC")
(gen/sample (s/gen (s/cat :0 zero? :ns (s/+ (s/and int? pos?)))))
; =>
; ((0 7)
;  (0 4)
;  (0 1 1 2)
;  (0 14 2 1 1)
;  (0 1 2 1 76)
;  (0 1 5 3 112 1)
;  (0 14 272 3)
;  (0 93 7 1 56 26 7 6)
;  (0 10 9 5 106 359 325 1 1)
;  (0 4 14 61 14 27 112 3 14 5))

(s/exercise (s/cat :0 zero? :ns (s/+ (s/and int? pos?))) 3)
; =>
; ([(0 8) {:0 0, :ns [8]}]
;  [(0 21) {:0 0, :ns [21]}]
;  [(0 17) {:0 0, :ns [17]}]
;  [(0 24 1) {:0 0, :ns [24 1]}]
;  [(0 3) {:0 0, :ns [3]}])

(s/def ::nat (s/and int? pos?))
(defn gen-row [m] (s/gen (s/coll-of ::nat :count m)))
(defn gen-matrix [n m] (gen/vector (gen-row m) n))

(gen/sample (gen-matrix 2 3) 5)
; =>
; ([[1 3 5] [11 1 1]]
;  [[2 3 4] [5 2 1]]
;  [[1 1 1] [1 1 1]]
;  [[6 1 15] [24 1 1]]
;  [[3 1 15] [246 2 6]])

(s/def ::w42 (s/and string? #(re-find #"42" %)))
;(gen/sample (s/gen ::w42))
; Error printing return value (ExceptionInfo) at clojure.test.check.generators/fn (generators.cljc:435).
; Couldn't satisfy such-that predicate after 100 tries.

(def with-42 (gen/fmap #(str (first %) 42 (second %))
                       (gen/tuple (s/gen string?) (s/gen string?))))

(gen/sample with-42 5)
; => ("42" "424" "J6422" "H7242n9d" "3R42")