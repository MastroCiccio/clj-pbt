# Clojure - Macro

Le macro sono il meccanismo che permette di trasformare espressioni arbitrarie in codice Clojure valido.
Per farlo, manipolano le strutture dati che rappresentano il codice stesso!

### Il modello di valutazione di Clojure

Il modello si articola in due fasi:
1. parsing del codice in formato testuale nelle strutture dati ([S-expressions](https://en.wikipedia.org/wiki/S-expression)) di Clojure
2. valutazione delle strutture dati

In clojure, tutto è rappresentato tramite liste, anche il codice stesso ([Homoiconicity](https://en.wikipedia.org/wiki/Homoiconicity))!
Un AST è facilmente rappresentabile usando delle liste innestate.
Durante la prima fase del modello il codice sorgente è quindi tradotto in un AST implementato tramite liste. 
Tale AST è accessibile dal programma stesso ed è possibile valutarne l'output chiamando esplicitamente il valutatore con `eval`.

Ad esempio, l'espressione `(list + 2 '(* 5 8))` viene tradotta nell'albero:
```
   +
  / \
 2   *
    / \
   5   8
```

L'albero può essere quindi valutato: `(eval (list + 2 '(* 5 8)))`, l'output è quello atteso `42`.

Le macro si pongono tra le due fasi del modello, tra reader ed evaluator. 
Permettono di scrivere codice che genera nuovo codice manipolando l'AST. 
Dato che l'AST è rappresentato tramite liste (native in Clojure), non sono nemmeno richieste skill aggiuntive (più o meno).

```clojure
(defmacro backwards
  [form]
  (reverse form))

(backwards ("contrario" "al " "Stringa " str))
; => "Stringa al contrario"
``` 

Più nello specifico, le macro si comportano in modo molto simile alle funzioni: accettano un elenco di parametri in input,
ritornano un valore in output e manipolano le strutture dati di Clojure nel loro body.
A differenza delle funzioni, tuttavia, le macro non valutano i parametri in input (i simboli di funzione non sono risolti!),
ma valutano il valore di ritorno.

```clojure
(defmacro ignore-last-operand
  [function-call]
  (butlast function-call))

(ignore-last-operand (+ 1 2 10))
; => 3

;; Nessuna stampa
(ignore-last-operand (+ 1 2 (println "look at me!!!")))
; => 3
```

Nell'esempio precedente, l'input alla macro non viene valutato, ma passato esattamente così per come è. Il valore di ritorno,
ovvero `(+ 1 2)` viene valutato ritornando `3`.

Ecco perché le macro si pongono tra `reader` e `evaluator`: i parametri in ingresso sono il risultato del parsing del codice
nell'AST; nel corpo della macro avvengono le manipolazioni dell'AST (sono sempre liste!); infine, ciò che la macro ritorna 
in output viene dato in pasto all'`evaluator` che produce il risultato finale.

Data la loro flessibilità, le macro sono uno strumento molto potente che permette di estendere il linguaggio nel modo che più
si ritieni opportuno. Ad esempio:

```clojure
(defn sum-evens
  []
  (reduce + (filter even? (repeatedly 10 #(rand-int 100)))))

;; Così è un po' più leggibile no?
(defn sum-evens
  []
  (->> (repeatedly 10 #(rand-int 100))
       (filter even?)
       (reduce +)))
```

A ulteriore dimostrazione dell'importanza delle macro, il core stesso di Clojure ne fa grande uso.
Alcuni esempi: `when`, `cond`, `if-let`, `doseq`, `and`, `or`.
La logica fornita da ciascuna di queste macro non potrebbe essere replicata con delle normali funzioni!